#pragma once

#include "stdint.h"

namespace GraphicEngine
{
    using KeyCode = uint16_t;
    
    // AZERTY KEY CODES
    namespace Key
    {
        enum : KeyCode
        {
            CTRL_LEFT   = 341,
            CTRL_RIGHT  = 345,
            SHIFT_LEFT  = 340,
            SHIFT_RIGHT = 344,
            ALT_LEFT    = 342,
            ALT_RIGHT   = 346,

            LEFT_KEY    = 263,
            UP_KEY      = 265,
            RIGHT_KEY   = 262,
            DOWN_KEY    = 264,

            SPACE_KEY   =  31,
            ENTER_KEY   = 257,
            ESC_KEY     = 256,
            
            A_KEY       =  81,
            B_KEY       =  66,
            C_KEY       =  67,
            D_KEY       =  68,
            E_KEY       =  69,
            F_KEY       =  70,
            G_KEY       =  71,
            H_KEY       =  72,
            I_KEY       =  73,
            J_KEY       =  74,
            K_KEY       =  75,
            L_KEY       =  76,
            M_KEY       =  59,
            N_KEY       =  78,
            O_KEY       =  79,
            P_KEY       =  80,
            Q_KEY       =  65,
            R_KEY       =  82,
            S_KEY       =  83,
            T_KEY       =  84,
            U_KEY       =  85,
            V_KEY       =  86,
            W_KEY       =  90,
            X_KEY       =  88,
            Y_KEY       =  89,
            Z_KEY       =  87
        };
    }
}